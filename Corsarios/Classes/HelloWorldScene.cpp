#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include <unistd.h>
#include "MainScene.h"

using namespace CocosDenshion;

USING_NS_CC;

#define PEW_PEW_SFX           "res/cannon_explosion.mp3"

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    SimpleAudioEngine::getInstance()->playEffect(PEW_PEW_SFX, false, 1.0f, 1.0f, 1.0f);
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
    
    closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width*2 ,
                                origin.y + closeItem->getContentSize().height*2));
    
    closeItem->setScaleX(4);
    closeItem->setScaleY(4);
    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
    auto label = Label::createWithTTF("Welcome Pirates!!", "fonts/Marker Felt.ttf", 24);
    
    // position the label on the center of the screen
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - label->getContentSize().height));

    // add the label as a child to this layer
    
    this->addChild(label, 1);
    label->setVisible(false);
    auto  fadeIn = FadeIn::create(3.0f);
    label->runAction(fadeIn);
label->setVisible(true);
    // add "HelloWorld" splash screen"
    auto sprite = Sprite::create("res/cannon.png");

    // position the sprite on the center of the screen
    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    // add the sprite as a child to this layer
    this->addChild(sprite, 2);
    
    
    // add "HelloWorld" splash screen"
    auto sprite2 = Sprite::create("res/big_explosion.png");
    
    // position the sprite on the center of the screen
    sprite2->setPosition(Vec2(visibleSize.width/2 + origin.x - sprite->getPositionX()/3, visibleSize.height/2 + origin.y + 10));
    sprite2->setScaleX(4);
    sprite2->setScaleY(4);

    // add the sprite as a child to this layer
    this->addChild(sprite2, 3);
    auto fadeIn2 = FadeIn::create(1.0f);
   
    
    // fades out the sprite in 2 seconds
    auto fadeOut = FadeOut::create(2.0f);
   
    auto seq = Sequence::create(fadeIn2,fadeOut, nullptr);
    sprite2->runAction(seq);
    
  //   auto scene = MainScene::createScene();
   // Director::getInstance()->replaceScene(TransitionFlipX::create(2, scene));
    auto action = Sequence::create(DelayTime::create(5), CallFunc::create(this, callfunc_selector(HelloWorld::transitionToGameScene)), NULL);
    this->runAction(action);
       return true;
}

void HelloWorld::transitionToGameScene(){
    auto size = Director::getInstance()->getWinSize();      //get the windows size.
    
    auto clipper = ClippingNode::create();      // create the ClippingNode object
    
    auto stencil = DrawNode::create();      // create the DrawNode object which can draw dots, segments and polygons.
    
    Point triangle[3];      // init the  triangle vertexes. here my win size is 360x640, so my triangle vertexes init by these values. You can change the values to adapt your scree.
    triangle[0] = Point(-size.width * 1.5f, -size.height / 2);
    triangle[1] = Point(size.width * 1.5f, -size.height / 2);
    triangle[2] = Point(0, size.height);
    Color4F green(0, 1, 0, 1);
    
    stencil->drawPolygon(triangle, 3, green, 0, green);     //use the drawNode to draw the triangle to cut the ClippingNode.
    
    clipper->setAnchorPoint(Point(0.5f, 0.5f));     // set the ClippingNode anchorPoint, to make sure the drawNode at the center of ClippingNode
    clipper->setPosition(size.width / 2, size.height / 2);
    clipper->setStencil(stencil);   //set the cut triangle in the ClippingNode.
    clipper->setInverted(true);     //make sure the content is show right side.
    
    Sprite* blackRect = Sprite::create("res/black_background.png");     //create a black screen sprite to make sure the bottom is black. the"black_screen.png" is a "black screen" png.
    
    clipper->addChild(blackRect);   //to make sure the cover is black.
    
    this->addChild(clipper, 500);
    
    // the Clipping node triangle  add some actions to make the triangle scale and rotate.
    stencil->runAction(EaseSineOut::create(Spawn::create(ScaleTo::create(2.5f, 0.0f, 0.0f), RotateBy::create(2.5f, 540),
                                                         Sequence::create(DelayTime::create(2.5), CallFunc::create(this, callfunc_selector(HelloWorld::toGameScene)), NULL), NULL)));

}

void HelloWorld::toGameScene()
{
    //get the game scene and run it.
    auto scene = MainScene::createScene();
    Director::getInstance()->replaceScene(scene);
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
