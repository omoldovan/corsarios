#include "MainScene.h"
#include "SimpleAudioEngine.h"
#include "ui/CocosGUI.h"

using namespace CocosDenshion;
using namespace std;

USING_NS_CC;

#define BACKGROUND_MUSIC_SFX  "res/Crisp_Ocean_Waves-Mike_Koenig-1486046376.mp3"
#define PEW_PEW_SFX           "res/cannon_explosion.mp3"

enum class PhysicsCategory {
    None = 0,
    Ship_p = (1 << 0),    // 1
    Ship_o = (1 << 1), // 2
    Projectile_p = (1 << 2), // 3
    Projectile_o = (1 << 3), // 4
    All = PhysicsCategory::Ship_p |PhysicsCategory::Ship_o| PhysicsCategory::Projectile_p | PhysicsCategory::Projectile_o// 3
};

Scene* MainScene::createScene()
{
    auto scene = Scene::createWithPhysics();
    scene->getPhysicsWorld()->setGravity(Vec2(0.0f, -500.0f));
    scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    auto layer = MainScene::create();
    scene->addChild(layer);
    return scene;
}

// on "init" you need to initialize your instance
bool MainScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
    
    set_background();

    //start transition and game
    this->toGameScene();
    return true;
}

void MainScene::set_background(){

    //Set background
    background_sprite = Sprite::create("res/fondoBatalla.png");
    background_sprite->setPosition(Vec2(origin.x + visibleSize.width / _center, origin.y + visibleSize.height / _center));
    background_sprite->setScaleX((visibleSize.width / background_sprite->getContentSize().width) * scaleXFactor);
    background_sprite->setScaleY((visibleSize.height / background_sprite->getContentSize().height) * scaleYFactor);
    this->addChild(background_sprite,0);
}

void MainScene::close_menu(){
    closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(MainScene::menuCloseCallback, this));
    
    closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width*2                      ,origin.y + closeItem->getContentSize().height*2));
    closeItem->setScaleX(4);
    closeItem->setScaleY(4);
    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

}

bool MainScene::set_player(){
    //Player 1 Label
    auto p1_label = Label::createWithTTF("Player1", "fonts/Marker Felt.ttf", 24);
    p1_label->setPosition(Vec2(origin.x + visibleSize.width/_left,
                               origin.y + visibleSize.height - p1_label->getContentSize().height));
    this->addChild(p1_label, 2);
    
    lifeP1 = ProgressTimer::create(Sprite::create("res/rectangle.png"));
    if ( lifeP1 != NULL )
    {
        lifeP1->setType(ProgressTimer::Type::BAR);
        lifeP1->setMidpoint(Point(0, 0));
        lifeP1->setBarChangeRate(Point(1, 0));
        lifeP1->setPercentage(100);
        lifeP1->setPosition(Vec2(origin.x + visibleSize.width/_left,
                                 origin.y + visibleSize.height - p1_label->getContentSize().height * 2));
        lifeP1->setScaleX(0.2);
        lifeP1->setScaleY(0.1);
        this->addChild(lifeP1);
    }
    
    player= Sprite::create("res/ship.png");
    player->setPosition(Vec2(origin.x + visibleSize.width/_left,
                             origin.y + visibleSize.height - p1_label->getContentSize().height * _depth));
    player->setName("Player");
    
    playerX=player->getPositionX();
    playerY=player->getPositionY();
    
    auto shipSize = player->getContentSize();
    auto physicsBody = PhysicsBody::createBox(Size(shipSize.width , shipSize.height),
                                              PhysicsMaterial(0.1f, 1.0f, 0.0f));
    //2
    physicsBody->setDynamic(false);
    //3
    physicsBody->setCategoryBitmask((int)PhysicsCategory::Ship_p);
    physicsBody->setCollisionBitmask((int)PhysicsCategory::None);
    physicsBody->setContactTestBitmask((int)PhysicsCategory::Projectile_o);
    player->setPhysicsBody(physicsBody);
    
    
    // create a MoveBy Action to where we want the sprite to drop from.
    auto movedown = MoveBy::create(2, Vec2(0, player->getContentSize().height/20));
    auto move_back = movedown->reverse();
    
    
    // create a delay that is run in between sequence events
    auto delay = DelayTime::create(0.25f);
    
    // create the sequence of actions, in the order we want to run them
    auto seq1 = Sequence::create(movedown, delay, move_back,
                                 delay->clone(), nullptr);
    
    // run the sequence and repeat forever.
    player->runAction(RepeatForever::create(seq1));
    
    
    this->addChild(player, 3);
   
    
    //Sprite* diana_sprite;
    diana_sprite = Sprite::create("res/tinta.png");
    diana_sprite->setPosition(Vec2((origin.x + visibleSize.width/_left)*2,
                                   origin.y + visibleSize.height - p1_label->getContentSize().height * _depth));
    diana_sprite->setScaleX(0.2);
    diana_sprite->setScaleY(0.2);
    
    this->addChild(diana_sprite, 5);
    player->runAction(RepeatForever::create(seq1->clone()));
    
    
    return true;
}

bool MainScene::set_oponent(){
    ran_y=cocos2d::random(0, 1);
    
    //Player 2 Label
    auto p2_label = Label::createWithTTF("Player2", "fonts/Marker Felt.ttf", 24);
    p2_label->setPosition(Vec2(origin.x + visibleSize.width/_right,
                               origin.y + visibleSize.height - p2_label->getContentSize().height));
    this->addChild(p2_label, 2);
    
    lifeP2 = ProgressTimer::create(Sprite::create("res/rectangle.png"));
    if ( lifeP2 != NULL )
    {
        lifeP2->setType(ProgressTimer::Type::BAR);
        lifeP2->setMidpoint(Point(0, 0));
        lifeP2->setBarChangeRate(Point(1, 0));
        lifeP2->setPercentage(100);
        lifeP2->setPosition(Vec2(origin.x + visibleSize.width/_right,
                                 origin.y + visibleSize.height - p2_label->getContentSize().height * 2));
        lifeP2->setScaleX(0.2);
        lifeP2->setScaleY(0.1);
        this->addChild(lifeP2);
    }
    
    oponent = Sprite::create("res/ship_reverse.png");
    oponent->setPosition(Vec2(origin.x + visibleSize.width/_right,
                              origin.y + visibleSize.height - p2_label->getContentSize().height * _depth));
    oponent->setName("Oponent");
    auto shipSize = oponent->getContentSize();
    
    auto physicsBody2 = PhysicsBody::createBox(Size(shipSize.width , shipSize.height),
                                               PhysicsMaterial(0.1f, 1.0f, 0.0f));
    //2
    physicsBody2->setDynamic(false);
    //3
    physicsBody2->setCategoryBitmask((int)PhysicsCategory::Ship_o);
    physicsBody2->setCollisionBitmask((int)PhysicsCategory::None);
    physicsBody2->setContactTestBitmask((int)PhysicsCategory::Projectile_p);
    
    oponent->setPhysicsBody(physicsBody2);
    // create a MoveBy Action to where we want the sprite to drop from.
    auto movedown_2 = MoveBy::create(2, Vec2(0, oponent->getContentSize().height/20));
    auto move_back_2 = movedown_2->reverse();
    
    // create a delay that is run in between sequence events
    auto delay = DelayTime::create(0.25f);
    
    // create the sequence of actions, in the order we want to run them
    seq2 = Sequence::create(movedown_2, delay, move_back_2,
                            delay->clone(), nullptr);
    
    // run the sequence and repeat forever.
    oponent->runAction(RepeatForever::create(seq2));
    
    
    this->addChild(oponent, 4);
    
    oponentY=oponent->getPositionY();
    
    oponentX=oponent->getPositionX();
    
    diana_sprite2 = Sprite::create("res/tinta.png");
    
    diana_sprite2->setPosition(Vec2(Vec2(origin.x + visibleSize.width/_right -oponent->getContentSize().width,
                                         origin.y + visibleSize.height - oponentY*2 -oponent->getContentSize().height/5*ran_y  )));
    
    
    diana_sprite2->setScaleX(0.2);
    diana_sprite2->setScaleY(0.2);
    
    
    this->addChild(diana_sprite2, 2);
    diana_sprite2->runAction(RepeatForever::create(seq2->clone()));
    //diana_sprite2->setVisible(false);
    
    return true;
}
void MainScene::sound_item(){
    //sound button
    sound_button= ui::Button::create("res/sound.png","res/sound_on.png","res/sound_off.png");
    
    sound_button->setTitleText("sound");
    
    sound_button->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width*6                      ,origin.y + closeItem->getContentSize().height*2));
    
    sound_button->setScaleX(0.2);
    sound_button->setScaleY(0.2);
    
    sound_button->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                if (this->sound == true){
                    this->sound_off();
                }
                else {
                    this->sound_on();
                }
                break;
            default:
                break;
        }
    });
    this->addChild(sound_button);
    
    
}
void MainScene::set_shoot_buton(){
    shoot_button = ui::Button::create("res/fire_button.png","res/fire_button_press.png","res/fire_button_dis.png");
    
    shoot_button->setTitleText("FIRE!!!");
    shoot_button->setPosition(Vec2(origin.x + visibleSize.width /_left                   ,origin.y + shoot_button->getContentSize().height));
    
    shoot_button->setScaleX(0.4);
    shoot_button->setScaleY(0.4);
    
    shoot_button->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                this->fire();
                break;
            default:
                break;
        }
    });
    
    this->addChild(shoot_button);    //Sprite* cannon_sprite;

}
void MainScene::set_slider_power(){
    
    slider = cocos2d::ui::Slider::create();
    slider->loadBarTexture("res/Slider_Back.png"); // what the slider looks like
    slider->loadSlidBallTextures("res/SliderNode_Normal.png", "res/SliderNode_Press.png", "res/SliderNode_Disable.png");
    slider->loadProgressBarTexture("res/Slider_PressBar.png");
    slider->setPosition(Point(visibleSize.width/2,(visibleSize.height/4)*1));
    slider->setPercent(50);
    slider->setVisible(true);
    slider->setScale(4);
    slider->setName("Slider");
    
    slider->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                if (slider->getPercent() >= 0){
                    velocidad = slider->getPercent();
                    
                }
                else{
                    CCLOG("null slider");
                }
                break;
            default:
                break;
        }
    });
    
    this->addChild(slider,11);
    
}


bool MainScene::sound_off(){
    this->sound=false;
    auto audio = SimpleAudioEngine::getInstance();
    audio->stopBackgroundMusic();
    return true;
}


bool MainScene::sound_on(){
    this->sound=true;
    auto audio = SimpleAudioEngine::getInstance();
    audio->setEffectsVolume(2.0f);
    SimpleAudioEngine::getInstance()->playBackgroundMusic(BACKGROUND_MUSIC_SFX , true);
    return true;
}


void MainScene::startGame(){
    //start music
    this->sound_on();
    
    //set visual elements
    this->close_menu();
    this->set_player();
    this->set_oponent();
    this->sound_item();
    this->set_shoot_buton();
    this->set_slider_power();
    
    //start listeners
    auto eventListener = EventListenerTouchOneByOne::create();
    eventListener->onTouchBegan = CC_CALLBACK_2(MainScene::onTouchBegan, this);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(eventListener, this);
    
    auto contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin = CC_CALLBACK_1(MainScene::onContactBegan, this);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);
}


bool MainScene::fire(){
    //set shot button visible
    shoot_button->setVisible(false);

    //set sprite projectile and physics
    projectile = Sprite::create("res/bola.png");
    projectile->setName("Projectile_p");
    projectile->setScaleX(0.2);
    projectile->setScaleY(0.2);
    projectile->setPosition(player->getPosition());
  
    auto projectileSize = projectile->getContentSize();

    auto physicsBody = PhysicsBody::createCircle(projectileSize.width/2 ,PhysicsMaterial(0.1f, 1.0f, 0.0f));
    
    physicsBody->setGravityEnable(true);
    
    physicsBody->setDynamic(true);
    physicsBody->setCategoryBitmask((int)PhysicsCategory::Projectile_p);
    physicsBody->setCollisionBitmask((int)PhysicsCategory::None);
    physicsBody->setContactTestBitmask((int)PhysicsCategory::Ship_o);
    
    physicsBody->setVelocity(Vec2(cocos2d::random(-1 * velocidad,velocidad),
                                  cocos2d::random(-1 * velocidad,velocidad)));
    projectile->setPhysicsBody(physicsBody);
    this->addChild(projectile);
    
    //manage projectile trajectory
    this->manage_fire();
    return true;
}

bool MainScene::manage_fire(){
    Vec2 touchLocation = diana_sprite->getPosition();
    Vec2 offset = touchLocation - player->getPosition();
    if (offset.x < 0) {
        return true;
    }
    
    offset.normalize();
    auto shootAmount = offset * 1000;
    auto realDest = shootAmount + projectile->getPosition();
    auto actionMove = MoveTo::create(2.0f, realDest);
    auto actionRemove = RemoveSelf::create();
    
    auto callback = CallFunc::create( this, callfunc_selector(MainScene::responseFire) );
    
    projectile->runAction(Sequence::create(actionMove,actionRemove, callback, nullptr));
    
    SimpleAudioEngine::getInstance()->playEffect(PEW_PEW_SFX, false, 1.0f, 1.0f, 1.0f);
    diana_sprite2->setVisible(true);

    return true;
}


bool MainScene::onTouchBegan(Touch *touch, Event *unused_event) {
    Vec2 touchLocation = touch->getLocation();
    Vec2 offset = touchLocation - player->getPosition();
    if (offset.x < 0) {
        return true;
    }
    diana_sprite->setPosition(diana_sprite->getPositionX(), touchLocation.y);

    return true;
}

bool MainScene::fade_explosion(){
    auto explosion= Sprite::create("res/big_explosion.png");
    explosion->setPosition(Vec2(nodeB->getPosition()));
    explosion->setName("Explosion");
    this->addChild(explosion, 8);
    
    auto fadeOut = FadeOut::create(2.0f);
    explosion->runAction(fadeOut);
    return true;
}


bool MainScene::hit_move(){
    auto rotateBy = RotateBy::create(1.0f, 10.0f);
    auto rotateBy2 = RotateBy::create(1.0f, -20.0f);
    auto rotateBy3 = RotateBy::create(1.0f, 10.0f);
    
    auto seq = Sequence::create(rotateBy,rotateBy2,rotateBy3, nullptr);
    nodeB->runAction(seq);
    return true;
}


bool MainScene::bulls_eyes()
{
    this->fade_explosion();
    
    this->hit_move();
    
    if (nodeB->getName() == "Oponent" ||nodeA->getName() == "Oponent"){
        if (lifeP2->getPercentage() > 0){
            lifeP2->setPercentage(lifeP2->getPercentage() -20+cocos2d::random(0,10));
            responseFire();
        }
        else
        {
            end_fire= Sprite::create("res/fire3.png");
            end_fire->setPosition(Vec2(nodeB->getPosition()));
            this->addChild(end_fire, 9);
            end_game("Player");
        }
    }
    else{
        if (lifeP1->getPercentage() > 0){
            lifeP1->setPercentage(lifeP1->getPercentage() -20+cocos2d::random(0,10));
            startButton();
        }
        else
        {
            end_fire= Sprite::create("res/fire3.png");
            end_fire->setPosition(Vec2(nodeB->getPosition()));
            this->addChild(end_fire, 9);
            end_game("You lose!!!");
            
        }
    }
    
    return true;
}

bool MainScene::onContactBegan(PhysicsContact &contact) {
 
    nodeA = contact.getShapeA()->getBody()->getNode();
    nodeB = contact.getShapeB()->getBody()->getNode();
    nodeA->removeFromParent();
    
    this->bulls_eyes();
    
    return true;
}


bool MainScene::set_end_label(char* text){
    end_label = Label::createWithTTF("GAME OVER!!!", "fonts/Marker Felt.ttf", 34);
    end_label->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height/2));
    this->addChild(end_label, 2);
    
    
    winner_label = Label::createWithTTF(text, "fonts/Marker Felt.ttf", 34);
    winner_label->setPosition(Vec2(origin.x + visibleSize.width/2,
                                   origin.y + visibleSize.height/2 - end_label->getContentSize().height));
    winner_label->setColor(ccc3(255,215,0));
    this->addChild(winner_label, 2);
    
    return true;
}


bool MainScene::end_game(String winner){
    
    auto callback3 = CallFunc::create( this, callfunc_selector(MainScene::game_menu ));
    
    if ( winner.compare("Player")==0){
        this->set_end_label("You win!!!");
        auto rotateBy = RotateBy::create(5.0f, 90.0f);
     
        oponent->runAction(Sequence::create(rotateBy,callback3,nullptr));
    }
    else{
        this->set_end_label("You lose!!!");
        auto rotateBy = RotateBy::create(5.0f, 90.0f);
        player->runAction(Sequence::create(rotateBy,callback3,nullptr));
    }

    shoot_button->setEnabled(false);
    sound_off();
    
    return true;
}

void MainScene::reset_start(){
    
    end_label->removeFromParent();
    winner_label->removeFromParent();
    slider->setVisible(false);

}

void MainScene::game_menu(){
    
    this->reset_start();
    auto menu_item1 = MenuItemFont::create("Play again!",CC_CALLBACK_1(MainScene::play, this));
    auto menu_item2 = MenuItemFont::create("HighScores",CC_CALLBACK_1(MainScene::highScores, this));
    auto menu_item3 = MenuItemFont::create("Settings",CC_CALLBACK_1(MainScene::settings, this));
    
    menu_item1->setPosition(Point(visibleSize.width/2,(visibleSize.height/4)*3));
    menu_item2->setPosition(Point(visibleSize.width/2,(visibleSize.height/4)*2));
    menu_item3->setPosition(Point(visibleSize.width/2,(visibleSize.height/4)*1));
    
    menu_item1->setColor(ccc3(255,0,0));
    menu_item2->setColor(ccc3(255,255,0));
    menu_item3->setColor(ccc3(0,0,255));
    
    menu=Menu::create(menu_item1,menu_item2,menu_item3,NULL);
    menu->setPosition(0,0);
    this->addChild(menu);
    
}


void MainScene::play(Ref *pSender){
    
    CCLOG("Play");
    
    lifeP1->setPercentage(100);
    lifeP2->setPercentage(100);
    
    shoot_button->setEnabled(true);
    shoot_button->setVisible(true);
    
    end_fire->removeFromParent();
    
    player->setRotation(0);
    oponent->setRotation(0);
    
    menu->removeFromParent();
    slider->setVisible(true);
}

void MainScene::highScores(Ref *pSender)
    {
        
        CCLOG("Scores");
        cocos2d::MessageBox("Nothing", "No scores yet");
        
    }


void MainScene::settings(Ref *pSender){
    
    CCLOG("Settings");
    cocos2d::MessageBox("Nothing", "No settings");
    
}

bool MainScene::manage_response(){
    
    Vec2 offset =  diana_sprite2->getPosition() - Vec2(oponentX,oponentY) ;
    if (offset.y < 0) {
        //return;
    }
    
    offset.normalize();
    auto shootAmount = offset * 1000;
    auto realDest = shootAmount + projectile2->getPosition();
    auto actionMove2 = MoveTo::create(2.0f, realDest);
    auto actionRemove2 = RemoveSelf::create();
    
    auto callback2 = CallFunc::create( this, callfunc_selector(MainScene::startButton) );
    
    projectile2->runAction(Sequence::create(actionMove2,actionRemove2,callback2, nullptr));
    SimpleAudioEngine::getInstance()->playEffect(PEW_PEW_SFX, false, 1.0f, 1.0f, 1.0f);
    

    return true;
}


void MainScene::responseFire(){
    
    diana_sprite2->setVisible(true);
    ran_y=cocos2d::random(0, 1);
    
    projectile2 = Sprite::create("res/bola.png");
    projectile2->setName("Projectile_r");
    projectile2->setScaleX(0.2);
    projectile2->setScaleY(0.2);
    projectile2->setPosition(Vec2(oponentX,oponentY));
    auto projectile2Size = projectile2->getContentSize();
    auto physicsBody = PhysicsBody::createCircle(projectile2Size.width/2 ,PhysicsMaterial(0.1f, 1.0f, 0.0f));
     physicsBody->setGravityEnable(true);
    
    physicsBody->setDynamic(true);
    physicsBody->setCategoryBitmask((int)PhysicsCategory::Projectile_o);
    physicsBody->setCollisionBitmask((int)PhysicsCategory::None);
    physicsBody->setContactTestBitmask((int)PhysicsCategory::Ship_p);
    physicsBody->setVelocity(Vec2(cocos2d::random(-100,100),
                                  cocos2d::random(-100,100)));
    projectile2->setPhysicsBody(physicsBody);
    
    this->addChild(projectile2);

    this->manage_response();
    }
void MainScene::startButton(){
    diana_sprite2->setVisible(false);
    shoot_button->setVisible(true);
    //diana_sprite2->removeFromParent();
   
}

void MainScene::toGameScene(){
    auto size = Director::getInstance()->getWinSize();      // get the win size.
    
    auto clipper = ClippingNode::create();  // get the clipping node.
    
    auto stencil = DrawNode::create();  //get the DrawNode.
    
    Point triangle[3];      // init the triangle vertexes.![]()
    triangle[0] = Point(-size.width * 1.5f, -size.height/2);
    triangle[1] = Point(size.width * 1.5f, -size.height/2);
    triangle[2] = Point(0, size.height);
    Color4F green(0, 1, 0, 1);
    
    stencil->drawPolygon(triangle, 3, green, 0, green); // use the drawNode to draw the triangle.
    
    clipper->setAnchorPoint(Point(0.5f, 0.5f)); // set the ClippingNode anchorPoint, to make sure the drawNode at the center of ClippingNode
    clipper->setPosition(size.width/2, size.height/2);
    clipper->setStencil(stencil);   //set the cut triangle in the ClippingNode.
    clipper->setInverted(true);     //make sure the content is show right side.
    
    Sprite* blackRect = Sprite::create("res/black_background.png");
    
    clipper->addChild(blackRect);//to make sure the cover is black.
    
    this->addChild(clipper, 500);
    
    // the Clipping node triangle  run some actions to make the triangle cut scale and rotate.
    stencil->setScale(0.0f);
    stencil->runAction(EaseSineIn::create(Spawn::create(ScaleTo::create(2.5f, 1.0f, 1.0f), RotateBy::create(2.5f, 540),
                                                        Sequence::create(DelayTime::create(1.0), CallFunc::create(this, callfunc_selector(MainScene::startGame)), NULL) , NULL)));    //when the actions over ,it call the startGame function.
    
}


void MainScene::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

