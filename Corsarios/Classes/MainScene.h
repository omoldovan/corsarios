#ifndef __MAINSCENE_SCENE_H__
#define __MAINSCENE_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;


class MainScene : public cocos2d::Layer
{
private:
    Sprite* player,* oponent,* background_sprite,* diana_sprite,* diana_sprite2,* cannon_sprite,* end_fire,* projectile,*projectile2;
    Node *nodeA, *nodeB;
    Label* end_label, *winner_label;
    ui::Button* shoot_button,* sound_button;
    ProgressTimer* lifeP2,* lifeP1;
    
    Menu* menu;
    cocos2d::ui::Slider* slider;
    Sequence* seq2;
    int velocidad;
    bool sound=true;    
    Vec2 origin;
    cocos2d::Size  visibleSize;
    float oponentY=0, oponentX=0,playerY=0,playerX=0,scaleYFactor=1, scaleXFactor=1,_center=2, _right=1.2, _left=8, _depth=8, ran_y;
    MenuItemImage*  closeItem;
    
    bool fire();
    bool sound_off();
    bool sound_on();
    void responseFire();
    bool end_game(String winner);
    void game_menu();
    void startButton();
    
    void set_background();
    void close_menu();
    bool set_oponent();
    bool set_player();
    void sound_item();
    void set_shoot_buton();
    void set_slider_power();
    
    bool manage_fire();
    bool bulls_eyes();
    bool fade_explosion();
    void reset_start();
    bool set_end_label(char* text);
    bool hit_move();
    bool manage_response();
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(MainScene);
      
    bool onTouchBegan(Touch *touch, Event *unused_event);
    
    bool onContactBegan(PhysicsContact &contact);
    void toGameScene();
    
    void startGame();
    void play(Ref *pSender);
    
    void highScores(Ref *pSender);
    
    void settings(Ref *pSender);
    

};

#endif // __MAINSCENE_SCENE_H__
